from datetime import datetime

from app import db


class Tarea(db.Model):
    __tablename__ = 'tareas'
    id = db.Column(db.Integer, primary_key=True)
    texto = db.Column(db.Text, nullable=False)
    fecha_creacion = db.Column(db.DateTime, default=datetime.now)
    autor = db.Column(db.Integer, db.ForeignKey('usuarios.id', ondelete="CASCADE"), nullable=False)
    comentarios = db.relationship('Comentario', backref='tareas', passive_deletes=True)


    def __init__(self, texto, autor):
        self.texto = texto
        self.autor = autor



class Comentario(db.Model):
    __tablename__= 'comentarios'
    id = db.Column(db.Integer, primary_key=True)
    texto = db.Column(db.Text, nullable=False)
    fecha_creacion = db.Column(db.DateTime, default=datetime.now)
    autor = db.Column(db.Integer, db.ForeignKey('usuarios.id', ondelete="CASCADE"), nullable=False)
    tarea = db.Column(db.Integer, db.ForeignKey('tareas.id', ondelete="CASCADE"), nullable=False)


    def __init__(self, texto, autor, tarea):
        self.texto = texto
        self.autor = autor
        self.tarea = tarea