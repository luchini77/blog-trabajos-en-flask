from flask import Blueprint, flash, redirect, url_for, render_template, request
from flask_login import login_required, current_user

from app.trabajos.modelo_trabajo import Tarea, Comentario
from app.auth.modelo_auth import Usuario
from app import db


tarea = Blueprint("tarea", __name__)


@tarea.route('/')
@tarea.route('/home')
def home():

    tareas = Tarea.query.order_by(Tarea.fecha_creacion.desc()).all()

    return render_template("trabajo/home.html", usuario=current_user, tareas=tareas)


@tarea.route('/crear_tarea', methods=['GET','POST'])
@login_required
def crear_tarea():
    
    if request.method == 'POST':
        texto = request.form.get('texto')

        if not texto:
            flash("La Tarea esta vacia!", category='error')
        else:

            tarea = Tarea(texto=texto, autor=current_user.id)
            db.session.add(tarea)
            db.session.commit()

            flash("Tarea Creada!", category='success')

            return redirect(url_for('tarea.home'))

    return render_template("trabajo/crear_tarea.html", usuario=current_user)


@tarea.route('/borrar_tarea/<int:id>', methods=['GET','POST'])
@login_required
def borrar_tarea(id):

    tarea = Tarea.query.filter_by(id=id).first()

    if not tarea:
        flash("La tarea no existe!", category='error')
    elif current_user.id != tarea.autor:
        flash("No tienes permiso para borrar la tarea!", category='error')
    else:
        db.session.delete(tarea)
        db.session.commit()

        flash("Tarea borrada!", category="success")

    return redirect(url_for('tarea.home'))


@tarea.route('/tareas/<usuario>', methods=['GET','POST'])
@login_required
def tareas(usuario):

    usuario = Usuario.query.filter_by(nombre=usuario).first()

    if not usuario:
        flash("No existe este usuario!", category='error')
        return redirect(url_for("tarea.home"))

    tareas = usuario.tareas

    return render_template("trabajo/tareas.html", usuario=current_user, tareas=tareas, nombre=usuario)


@tarea.route("/crear_comentario/<int:tarea_id>", methods=['POST'])
@login_required
def crear_comentario(tarea_id):

    texto = request.form.get('texto')

    if not texto:
        flash("El comentario esta vacio!", category='error')
    else:
        tarea = Tarea.query.filter_by(id=tarea_id)

        if tarea:
            comentario = Comentario(texto=texto, autor=current_user.id, tarea=tarea_id)
            db.session.add(comentario)
            db.session.commit()

        else:
            flash("La tarea no existe!", category='error')

    return redirect(url_for("tarea.home"))


@tarea.route("/borrar_comentario/<comentario_id>")
@login_required
def borrar_comentario(comentario_id):

    comentario = Comentario.query.filter_by(id=comentario_id).first()

    if not comentario:
        flash("Comentario no existe!", category='error')
    elif current_user.id != comentario.autor and current_user.id != comentario.tarea.autor:
        flash("No tienes permiso para borrar comentario", category='error')
    else:
        db.session.delete(comentario)
        db.session.commit()
        flash("Comentario borrado!", category='success')

    return redirect(url_for("tarea.home"))