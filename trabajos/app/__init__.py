from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager


app = Flask(__name__)

app.config.from_object('config.DevelopmentConfig')

db = SQLAlchemy(app)

migrate = Migrate(app, db)

login_manager = LoginManager()
login_manager.login_view = "auth.login"
login_manager.init_app(app)


from app.auth.view_auth import auth
from app.trabajos.view_trabajo import tarea


app.register_blueprint(auth, url_prefix="/")
app.register_blueprint(tarea, url_prefix="/")