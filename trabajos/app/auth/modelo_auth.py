from flask_login import UserMixin
from werkzeug.security import generate_password_hash

from app import db

class Usuario(UserMixin, db.Model):
    __tablename__ = 'usuarios'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(128))
    tareas = db.relationship('Tarea', backref='usuario', passive_deletes=True)
    comentarios = db.relationship('Comentario', backref='usuario', passive_deletes=True)


    def __init__(self, nombre, password):
        self.nombre = nombre
        self.password = generate_password_hash(password)


    def __repr__(self):
        return f"Usuario: {self.nombre}"
