from flask import Blueprint, render_template, redirect, url_for, request, flash
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.security import check_password_hash

from app.auth.modelo_auth import Usuario
from app import login_manager

auth = Blueprint("auth", __name__)


@login_manager.user_loader
def load_user(id):
    return Usuario.query.get(int(id))


@auth.route('/login', methods=['GET','POST'])
def login():

    if request.method == 'POST':
        nombre = request.form.get('nombre')
        password = request.form.get('password')

        usuario = Usuario.query.filter_by(nombre=nombre).first()

        if usuario:
            if check_password_hash(usuario.password, password):
                flash(f"Bienvenido {usuario.nombre}!", category='success')
                login_user(usuario, remember=True)
                return redirect(url_for("tarea.home"))
            else:
                flash("Contraseña incorrecta!", category='error')
        else:
            flash("El usuario no existe!", category='error')
    return render_template("auth/login.html", usuario=current_user)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for("tarea.home"))