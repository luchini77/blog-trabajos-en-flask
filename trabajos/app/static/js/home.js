(function (){
    const btn_eliminacion=document.querySelectorAll(".btn_eliminacion");

    btn_eliminacion.forEach((btn) => {
        btn.addEventListener("click",function(e){
            e.preventDefault();

            Swal.fire({
                title:"¿Seguro de eliminar la tarea?",
                showCancelButton:true,
                confirmButtonText:"Eliminar",
                confirmButtonColor:"#d33",
                backdrop:true,
                showLoaderOnConfirm:true,
                preConfirm: () => {
                    location.href = e.target.href;
                },
                alowOutsideClick: () => false,
                allowEscapeKey: () => false,
            })
        });
    })
})();