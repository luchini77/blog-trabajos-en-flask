class Config:
    DEBUG = True
    TESTING =True
    
    #BASE DATOS
    SQLALCHEMY_DATABASE_URI = "sqlite:////home/luchini77/Escritorio/Python-Web/practicas_mas_refinadas/en_flask/trabajos/datos/tarea.db"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    
    
class ProductionConfig(Config):
    DEBUG = False
    
    
class DevelopmentConfig(Config):
    SECRET_KEY = 'tareas_Programadas_rutinarias'